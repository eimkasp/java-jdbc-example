import javax.xml.crypto.Data;
import java.sql.*;


public class Main {

    public static void main(String[] args) {

        String labas = "surname";

        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection(
                        "jdbc:mysql://localhost:3306/java1?useSSL=false",
                        "root", "root");
                // For MySQL only
                // The format is: "jdbc:mysql://hostname:port/databaseName", "username", "password"

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement database = conn.createStatement();
        ) {
            String strSelect = "select name, " + labas + " from darbuotojai";
            ResultSet results = database.executeQuery(strSelect);

            int rowCount = 0;
            while(results.next()) {   // Move the cursor to the next row, return false if no more row
                String name = results.getString("name");
                String surname = results.getString("surname");
                System.out.println("Darbuotojas: "  + name + " " + surname);
                ++rowCount;
            }

            System.out.println("Total number of records = " + rowCount);

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
